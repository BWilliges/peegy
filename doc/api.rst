:orphan:

API
============

.. currentmodule:: peegy

.. toctree::

Experimental Design and Analysis
--------------------------------

.. currentmodule:: peegy.processing.tools.eeg_epoch_operators

.. automodule:: peegy.processing.tools.eeg_epoch_operators
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   et_covariance


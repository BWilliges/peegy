import multiprocessing as mp
import sys
from distutils.version import LooseVersion
import sphinx
import os
sys.path.append("../")
from peegy import io  # noqa: E402

# If extensions (or modules to document with autodoc) are in another directory,

# add these directories to sys.path here. If the directory is relative to the

# documentation root, use os.path.abspath to make it absolute, like shown here.

# sys.path.insert(0, os.path.abspath('.'))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.

# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be

# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom

# ones.
rst_epilog = '.. |version| replace:: {:}'.format('../VERSION')
project = 'pEEGy'
author = u'Jaime A. Undurraga'
version = open('../VERSION').read()
copyright = open('../LICENSE.txt').read()
release = open('../VERSION').read()

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx_gallery.gen_gallery',
    "sphinx_multiversion",
    'alabaster'
]
autosummary_generate = True

# sphinx-gallery configuration
sphinx_gallery_conf = {
    'doc_module': 'peegy',
    'backreferences_dir': os.path.join('generated'),
    'reference_url': {'peegy': None},
    'download_all_examples': False,
    'show_memory': True,
    'filename_pattern': '/example_',
    'capture_repr': ('_repr_html_', '__repr__')
    # 'autoDocstring.docstringFormat': "sphinx"
}
# Generate the plots for the gallery
plot_gallery = True
# theme settings
html_title = 'pEEGy'
html_short_title = 'pEEGy'
html_theme = 'alabaster'
html_static_path = ['img']
html_theme_options = {
    'logo': 'peegy.png',
    'logo_name': True
}

templates_path = ['../_templates']
html_sidebars = {
    '**': ['about.html',
           'navigation.html',
           'searchbox.html',
           'versioning.html']
 }

# settings for sphinx-multiversion
smv_tag_whitelist = r'^v\d+\.\d+\.\d+$'            # Include tags like "v2.1.1"
# smv_branch_whitelist = r'^(?=master).*$'      # Include "master"
smv_remote_whitelist = None
# smv_remote_whitelist = r'^(origin|upstream)$'  # Use branches from origin and upstream

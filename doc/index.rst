Welcome to pEEGy
================

Hi and welcome to pEEGy.

The aim of pEEGy is to facilitate and improve both analysis and reproducibility of EEG or
similar data by generating simple and comprehensible processing pipelines that can be a applied
consistently to many data sets.

We are still in an early stage, but we can already
offer a general framework to create processing pipelines to analyse
EEG or other similar data.
We also offer a number of statistical and simple visualization tools
to explore and process your data. You can also create your own processing pipeline
items that can be plugged into the processing pipeline.

One of the most useful tools that we offer is the possibility of exporting the results
into SQLITE database that can be further used to analyse the results, for example,
with R software.
By working in this way, you can avoid to a great extent human errors that can arise from
copy/paste operations or other bad practices that can lead to wrong results and
interpretation of results.
Whilst we can't not fully avoid these type of errors, pEEGy can certainly help to
reduce them.

Usage examples can be found in the Examples.

Sidekicks
---------

We have also created other tools for EEG experiments.
- If you have a `Biosemi <https://www.biosemi.com/>`_ EEG system, you can use
`BiosemiRealtime <https://gitlab.com/jundurraga/biosemi_real_time>`_ package
to allow realtime processing in the time- and frequency-domain.
- If use are running auditory EEG experiments such as auditory brainstem responses (ABRs), auditory steady-state
responses (ASSRs), frequency-following responses (FFRs), auditory change complex (ACC), and other auditory evoked
responses. We have a matlab toolbox `AEP_GUI <https://gitlab.com/jundurraga/ucl-matlab>`_ to generate and present
these stimuli in a reliable way.

.. toctree::
   :maxdepth: 2
   :hidden:

   auto_examples/index
   api
